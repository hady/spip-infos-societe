<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'infos_societe_description' => 'Renseigner des informations générales sur votre société comme l\'adresse, le numéro de téléphone, l\'email...',
	'infos_societe_nom' => 'Informations sur la société',
	'infos_societe_slogan' => '',
);
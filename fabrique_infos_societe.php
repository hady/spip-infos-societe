<?php

/**
 *  Fichier généré par la Fabrique de plugin v6
 *   le 2017-05-11 18:03:24
 *
 *  Ce fichier de sauvegarde peut servir à recréer
 *  votre plugin avec le plugin «Fabrique» qui a servi à le créer.
 *
 *  Bien évidemment, les modifications apportées ultérieurement
 *  par vos soins dans le code de ce plugin généré
 *  NE SERONT PAS connues du plugin «Fabrique» et ne pourront pas
 *  être recréées par lui !
 *
 *  La «Fabrique» ne pourra que régénerer le code de base du plugin
 *  avec les informations dont il dispose.
 *
**/

if (!defined("_ECRIRE_INC_VERSION")) return;

$data = array (
  'fabrique' => 
  array (
    'version' => 6,
  ),
  'paquet' => 
  array (
    'prefixe' => 'infos_societe',
    'nom' => 'Informations sur la société',
    'slogan' => '',
    'description' => 'Renseigner des informations générales sur votre société comme l\'adresse, le numéro de téléphone, l\'email...',
    'version' => '1.0.0',
    'auteur' => 'Hadrien',
    'auteur_lien' => '',
    'licence' => 'GNU/GPL',
    'categorie' => 'edition',
    'etat' => 'dev',
    'compatibilite' => '[3.1.0;3.1.*]',
    'documentation' => '',
    'administrations' => '',
    'schema' => '1.0.0',
    'formulaire_config' => 'on',
    'formulaire_config_titre' => 'Configuration des informations sur la société',
    'fichiers' => 
    array (
      0 => 'autorisations',
    ),
    'inserer' => 
    array (
      'paquet' => '',
      'administrations' => 
      array (
        'maj' => '',
        'desinstallation' => '',
        'fin' => '',
      ),
      'base' => 
      array (
        'tables' => 
        array (
          'fin' => '',
        ),
      ),
    ),
    'scripts' => 
    array (
      'pre_copie' => '',
      'post_creation' => '',
    ),
    'exemples' => '',
  ),
  'objets' => 
  array (
  ),
  'images' => 
  array (
    'paquet' => 
    array (
      'logo' => 
      array (
        0 => 
        array (
          'extension' => 'png',
          'contenu' => 'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAADtUlEQVR4nO2b3XGrMBCFvxIogRLcQdRB3EHcgdPBpYOkA9OB0wF0YDqADkwHvg+IzDVhV2DrhzuTM6MnxJ7d1Wq1EgjCIwNegT/AGaiAFrhNWmufnW3fV/vuf4kd8AFc+Gno2naxsnZRLXgAGXBkfnR9tdZybCoyMoaQvRLO8Gm7Ws7kjngjruFzjngLbuUMcoaElcrwaausTlGwZ/2od0AJvANGUDa3z95t324lx9XqFhQfKxRqrDH5E3y5ldGs4P14gk/FaaECNcNI+oaxspfocPJNfl5A2hDG8CkMyyLi7IvQNfI9Q5jGxrtDLy+R8OkgaEhboe1wJ8vPR4XvHYIbNlCIMOjgmhKrV4ccfan7YhvGj8jQE+SVlatRpQjbyshP4YqEaqmggyJkq8aPyNBzwmGJACn0e/wlvE/ud40tw+bGB3bIDrjiGMBCednXUqetLIUnDm2JFDm00W88KQZDJEnKtR55pHwgRoHmNeNRMW25unnkMQrHbDS3Qufao1IoSvl2AMhL449I0xKH8ayUNgU6z1xG4bpL6FJi8jn3RxSKUkUAPikX3JXI0ultqE1Owf163RHGeJBz22XskAkdbkQ8ZgqIHNm+DORNTxdd1XDomLdxD/KcfHgbuUGUKDnnS3iY4pAjFKQ8UIK8VhpP5K51P2QdMMIIXDXIBVDuiXwLDsgFroumoC9swQGaHr8O2Ioivw5IxCsWCXlkRVIkwQ7CL4NTpHCAEbhqiF8IpXCAWggVwsNQpXAKB5QCVwHyZqgNpEwKB7QC1x7ib4djOyBXuL4PR6VTkxB5ILYDpPl/d9olHYld8I/YDpBOu+5yXMxD0ZgOMArPj69cndCx8qxUTAdUAkc31znUhxGXwaEcYhSZs7ktQz6zfyYXpHKANPd7lA+khaLIoytCCgc89HEU9CiYTRwLENsBWkJXR3/EQRHQLhGQEBn63+qHpYJqRciFbTohQ7+fUK8RlqNPhYptOSFD/6+p54Gy3vWb3FYiwTXyN574idr1o2RL+h8ltTl/w8O2vnQQ3BiuscTGcYFepS8y6dRoOiWML0IFhmWXsUrfxOUC0jFBGt/kVqaW6IIaP8KVE6YRceT5CxNH1l2/C/5Ve4++RErJ8mSNeUG+MvNi+5xYf/2uJ8KVmX+VrVcqGLLVJPqb5cD6aPDZelaUt6GQMeywYjqit5xbKMS+kTFsQzvCGd5Zjk0ZPocdQzZec91Nao2VlbLifAoZQ3YuGAqqmvko6eyzL9t3T4SR/gtdTUcq7FnDGgAAAABJRU5ErkJggg==',
        ),
      ),
    ),
    'objets' => 
    array (
    ),
  ),
);